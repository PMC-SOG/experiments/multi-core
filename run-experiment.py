#!/usr/bin/env python3

import os
import argparse
from scripts import sbatch_generator as utils


def parse_arguments():
    """Creates the command line interface"""
    parser = argparse.ArgumentParser(
        description='Run an experiment on the local machine')

    parser.add_argument('--processes',
                        type=int,
                        nargs=1,
                        required=True,
                        help='number of process')

    parser.add_argument('--threads',
                        type=int,
                        nargs=1,
                        required=True,
                        help='number of threads')

    parser.add_argument('--tool',
                        type=str,
                        nargs=1,
                        required=True,
                        choices=['pmc-sog', 'pnml2lts-mc'],
                        help='model-checker tool')

    parser.add_argument('--model-name',
                        type=str,
                        nargs=1,
                        required=True,
                        help='Name of the model (e.g. philo)')

    parser.add_argument('--model-instance',
                        type=str,
                        nargs=1,
                        required=True,
                        help='Instance of the model (e.g. philo10)')

    parser.add_argument('--formula',
                        type=int,
                        nargs=1,
                        required=True,
                        help='Number of the property to verify')

    parser.add_argument('--tool-parameter',
                        nargs=1,
                        dest='parameters',
                        action='append',
                        help='Parameter of the tool')

    return parser.parse_args()


def create_mpi_run(command, nodes=1):
    """Returns string of the mpi command"""
    return f"mpirun -n {nodes} {command}"


if __name__ == '__main__':
    args = parse_arguments()

    # Get default paths
    paths = utils.create_default_paths()

    # Parse tool parameters
    tool = {"name": args.tool[0], "parameters": {}}
    for parameter in args.parameters:
        k, v = parameter[0].split('=')
        tool["parameters"][k] = v

    # Create command to be executed
    command = utils.tool_command(tool, args.threads[0], args.model_name[0],
                                 args.model_instance[0], args.formula[0],
                                 paths)

    # Use mpi to run experiments with more than 1 processes
    nodes = args.processes[0]
    if (nodes > 1):
        command = create_mpi_run(command, nodes)

    # Run command
    os.system(command)
