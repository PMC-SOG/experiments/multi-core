# SOG-Based Multi-Core LTL Model Checking

This repository hosts the results for the paper.

The experimental results were obtained on [Magi
cluster](http://magi.univ-paris13.fr/wiki/) of [University Sorbonne Paris
Nord](https://univ-paris13.fr/). We used the partition `COMPUTE` which has 40
processors (two Intel Xeon E5-2650 v3 at 2.30GHz) connected by an InfiniBand
network, and 64GB of RAM. A total of 5 models from the [Model Checking
Contest](https://mcc.lip6.fr/models.php) were used in our experiments:
*[Philosophers](models/philo/description.pdf)* (`philo`),
*[RobotManipulation](models/robot/description.pdf)* (`robot`),
*[SwimmingPool](models/spool/description.pdf)*
(`spool`), *[CircularTrains]((models/train/description.pdf))* (`train`), and
*[TokenRing](models/tring/description.pdf)* (`ring`).


## Clone this repository:
```
git clone https://depot.lipn.univ-paris13.fr/PMC-SOG/experiments/multi-core.git && cd multi-core
```

## Folder Structure

```
.
├── formulas                        # folder containing the LTL\X formulas
├── models                          # folder containing the models
├── paper-ISPA2020.pdf              # paper with appendix
├── results
│   ├── figures
│   │   ├── compare_multicore       # figures comparing the sequential and multi-core version of our tool
│   │   ├── compare_thread_library  # figures comparing the pthreads and lace version of out tool
│   │   ├── compare_tools           # figures comparing our tool and pnml2lts-mc
│   │   └── time-plots              # figures showing the performance of the tool for each formula
│   │   ├── explored-states         # figures showing the states explored by pnml2lts-mc for each formula
│   ├── pmc-sog.zip                 # results (logs) related to our model-checker
│   └── pnml2lts-mc.zip             # results (logs) related to the ltsmin model-checker
├── run-experiment.py               # script that run a experiment on the local machine
├── scripts
│   ├── csv_generator.py            # script that generates a csv file with the results from the logs
│   ├── formula-generator.py        # script that generates N random LTL\X formulas
│   └── plot-results.ipynb          # Python notebook that allows to generate the figures and interact with the results
│   └── plot-results.py             # script that generates the figures (if you do not want to open the notebook)
│   └── sbatch_formula_generator.py # script that generates the sbatch files to generate formulas on Magi cluster
│   └── sbatch_generator.py         # script that generates the sbatch files to run the experiments on Magi cluster
└── tools
    ├── pmc-sog                     # binary of our model-checker
    └── pnml2lts-mc                 # binary of the ltsmin model-checker
```

## Run experiments

In the following we provide the steps to run the experiments. We tested this
on Ubuntu 18.04.4 LTS.

### Install dependencies

  - `sudo apt install git python3`
  - `pmc-sog`: Please read the [installation notes](https://depot.lipn.univ-paris13.fr/PMC-SOG/mc-sog).
  - `pnml2lts-mc`: Download from [here](http://github.com/utwente-fmt/ltsmin/releases/download/v3.0.2/ltsmin-v3.0.2-linux.tgz).


The binaries `pmc-sog` and `pnml2lts-mc` must be copied in the `tools` folder.

### Local Run

To run the experiments on a local machine, the script `run-experiment.py` can
be used.

`./run-experiment.py --processes 1 --threads 8 --tool pmc-sog --model-name=philo --model-instance=philo10 --formula=1 --tool-parameter parallelisation=otfP --tool-parameter strategy="Cou99 (poprem)"`

```
./run-experiment.py -h
usage: run-experiment.py [-h] --processes PROCESSES --threads THREADS --tool
                         {pmc-sog,pnml2lts-mc} --model-name MODEL_NAME
                         --model-instance MODEL_INSTANCE --formula FORMULA
                         [--tool-parameter PARAMETERS]

Run an experiment on the local machine

optional arguments:
  -h, --help            show this help message and exit
  --processes PROCESSES
                        number of process
  --threads THREADS     number of threads
  --tool {pmc-sog,pnml2lts-mc}
                        model-checker tool
  --model-name MODEL_NAME
                        Name of the model (e.g. philo)
  --model-instance MODEL_INSTANCE
                        Instance of the model (e.g. philo10)
  --formula FORMULA     Number of the property to verify
  --tool-parameter PARAMETERS
                        Parameter of the tool
```

### Cluster (slurm) Run

- Run the script that generates slurm batch:
  - `./scripts/sbatch_generator.py`
- Execute the experiment with `sbtach`:
  - `sbatch slurm/experiments/pmc-sog/pmc-sog_otfL_couv99-default/philo/n1-th8.sbatch`

## Generate Figures

In the following we provide the steps to generate the figures. We tested this
on Ubuntu 18.04.4 LTS.

### Install dependencies
  - `sudo apt install git unzip wget libfuse2 libnss3 libgdk-pixbuf2.0-0 libgtk-3-0 libx11-xcb1 libxss1 libasound2 python3 python3-pip`
  - `pip3 install --user pandas plotly psutil requests`
  - `wget https://github.com/plotly/orca/releases/download/v1.3.1/orca-1.3.1.AppImage`
  - `chmod +x orca-1.3.1.AppImage`
  - `mv orca-1.3.1.AppImage /usr/local/bin/orca`

### Decompress results
  - `cd results`
  - `unzip -q pmc-sog.zip`
  - `unzip -q pnml2lts-mc.zip`
  - `cd ..`

### Generate Figures
  - `cd scripts`
  - `python3 csv_generator.py`
  - `python3 plot-results.py`

## Authors

- Chiheb Ameur Abid (Faculty of Sciences of Tunis, University of Tunis El Manar, 2092, Tunis, Tunisia)
- Kais Klai (University of Paris 13, Sorbonne Paris Cite CNRS UMR 7030 LIPN
- Jaime Arias (University of Paris 13, Sorbonne Paris Cite CNRS UMR 7030 LIPN)
- Hiba Ouni (University of Paris 13, Sorbonne Paris Cite CNRS UMR 7030 LIPN)

## Abstract

The model checking is one of the major techniques used in the formal
verification. This technique builds on an automatic procedure that takes a
model M of a system and a formula φ expressing a temporal property, and
decides whether the system satisfies the property (denoted by M |= φ). The
model checking technique is based on an exhaustive exploration of the state
space of the system and, thus suffers from the state space explosion problem:
it can happen that the verification process stops because of lack of time or
space. Among the existing solutions to tackle this problem the symbolic
observation graph (SOG) has been proposed as a reduced representation of the
reachability graph preserving linear temporal logic properties (LTL) i.e.
checking an LTL property on the SOG is equivalent to check it on the
originally (bigger) state space. The parallel construction of the SOG could
increase the speedup and scalability of model checking. In this paper, we
propose a new model checking algorithm built on a parallel construction of
the Symbolic Observation Graph. The SOG is adapted to allow the preservation
of both state and event-based LTL formulae i.e., the atomic propositions
involved in the formula to be checked could be either state-based or
event-based propositions. We implemented the proposed model checking
algorithm within a C++ prototype and compared our preliminary results with
the state of the art model checkers.
